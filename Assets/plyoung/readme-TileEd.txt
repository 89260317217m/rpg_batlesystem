TilEd
-----

TileEd is a 3D tile map editor/ placement tool. It helps speed up painting of game worlds when working with tiles or modular pieces that needs to align to a grid.

- Documentation: https://plyoung.github.io/tile-ed.html

- Support: http://forum.plyoung.com/c/tile-ed

- Update notes available at http://forum.plyoung.com/t/tileed-updates/


