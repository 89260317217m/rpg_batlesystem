﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player 
{
    public string _name;  // Имя игрока
    bool _playTurn; // Сейчас ход игрока?
    public List<GameObject> _playerUnits = new List<GameObject>();  // Юниты игрока

    public Player(string n, bool b) //Конструктор
    {
        _name = n;
        _playTurn = b;
    }
    
}
