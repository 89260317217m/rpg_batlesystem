﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

public class Unit : MonoCashed // Скрипт Юнита, его параметры и прочее.
{
    public object this[string propName]  // Публичный доступ ко всем атрибутам Данного класа
    {
        get
        {
            for (int i = 0; i < typeof(Unit).GetFields().Length; i++)
            {
                if (typeof(Unit).GetFields()[i].Name == propName)
                {
                    return typeof(Unit).GetFields()[i].GetValue(this);
                }
            }
            for (int i = 0; i < typeof(Unit).GetProperties().Length; i++)
            {
                if (typeof(Unit).GetProperties()[i].Name == propName)
                {
                    return typeof(Unit).GetProperties()[i].GetValue(this, null);
                }
            }

            Debug.Log("get:Error Unit not have atribute: " + propName);
            return null;
        }
        set
        {
            bool successSet = false;
            for (int i = 0; i < typeof(Unit).GetFields().Length; i++)
            {
                if (typeof(Unit).GetFields()[i].Name == propName)
                {
                    typeof(Unit).GetFields()[i].SetValue(this, value);
                    successSet = true; break;
                }
            }
            for (int i = 0; i < typeof(Unit).GetProperties().Length; i++)
            {
                if (typeof(Unit).GetProperties()[i].Name == propName)
                {
                    typeof(Unit).GetProperties()[i].SetValue(this, value, null);
                    successSet = true; break;
                }
            }

            if (!successSet) Debug.Log("set:Error Unit not have atribute: " + propName);
        }
    }

    bool _endTurn; public bool EndTurn  // Юнит закончил ход и ничего не делает
    { get { return _endTurn; } set { _endTurn = value; } }

    public int _pointsAction; public int PointsAction  // Очки движения юнита
    {
        get { return _pointsAction; }
        set
        {
            EndTurn = value < 1 ? true : false; // Если нет очков - юнит отходил
            _pointsAction = value;
        }
    }

    public Player _playerUnit;

    public int _pointsHealth;  // Жизней

    public int _pointsToAttack;  // Очки затрачиваемые на атаку
    public int _pointsDistanceAttack;  // Дистанция атаки юнита
    public int _pointsZoneAttack;  // зона поражения от центра атаки

    public int _pointsStrengthAttack;  // Сила Атаки

    //public float _percentTop = 80;  // Для топовой атаки от очков действия
    //public float _percentMiddle = 35;  // Для средней атаки от очков действия
    //public float _percentLow = 25;  // Для слабой атаки от очков действия

    public Vector3 _posOnArray; // позиция в матрице поля боя // Хранит данные  для юнита - в каком секторе находится объект
    public Sector _unitOnSector; // ссылка в каком секторе находится объект

    public List<Vector3> _pathToMoveOnArray = new List<Vector3>(); // Путь по матрице карты
    public bool _unitIsMoving = false;
    float _speed = 4.0f;  // Скорость анимации перемещения

    private void Awake()
    {
        EventSys.Instance._changeUnitParam += UnitChangeParam;// подписываемся на событие 
    }

    void UnitChangeParam(GameObject toUnit, string paramName, int addValue)   // Меняем значения в полях Юнита   - этот метод подписан на событие!!! Слушаем события!!!   
    {
        if (toUnit == this.gameObject)  // Если событие для этого юнита
        {
            this[paramName] = (int)this[paramName] + addValue;
        }
    }

    private void Update()
    {
        if (_pathToMoveOnArray.Count > 0 & _unitIsMoving)  // Если есть путь и юнит может пойти - перемещаемся.
        {
            Vector3 nextPointOnMap = BattlefieldArray.Instance.ArrayToMapPosition(_pathToMoveOnArray[_pathToMoveOnArray.Count - 1].x, _pathToMoveOnArray[_pathToMoveOnArray.Count - 1].y, _pathToMoveOnArray[_pathToMoveOnArray.Count - 1].z);
            BattlefieldArray.Instance._array3D[(int)_posOnArray.x, (int)_posOnArray.y, (int)_posOnArray.z]._objectsOnSector.Remove(this.gameObject); // Говорим сектору что ушли с него
            transform.position = Vector3.MoveTowards(transform.position, nextPointOnMap, _speed * Time.deltaTime); // Движемся к первому шагу
            if (transform.position == nextPointOnMap) // Если дошел до точки Убираем её из списка пути
            {
                _posOnArray = _pathToMoveOnArray[_pathToMoveOnArray.Count - 1]; //Указываем в юните  его позицию в массиве
                _unitOnSector = BattlefieldArray.Instance._array3D[(int)_posOnArray.x, (int)_posOnArray.y, (int)_posOnArray.z]; // Добавляем ссылку на сектор в котором юнит
                BattlefieldArray.Instance._array3D[(int)_posOnArray.x, (int)_posOnArray.y, (int)_posOnArray.z]._objectsOnSector.Add(this.gameObject); // Говорим сектору что пришли на него
                _pathToMoveOnArray.RemoveAt(_pathToMoveOnArray.Count - 1);
                EventSys.Instance.SendEvent(this.gameObject, "PointsAction", -1); // Вызываем событие
                if (_pathToMoveOnArray.Count == 0) // Закончили движение юнита по карте
                {
                    _unitIsMoving = false;
                }
            }
        }
    }
}
