﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using plyLib;
using TileEd;

//using System.Data;
//using System;
//using System.Data.DataTableCollection;

//public class Sector : ScriptableObject   // Хранит данные о секторе  в массиве поля боя(BattlefieldArray)
public class Sector : MonoCashed   // Хранит данные о секторе  в массиве поля боя(BattlefieldArray)
{

    public string _meta; // Данные об плитке с построителя карты
    public GameObject _tileOnSector;  // Объект плитки (На основе префаба) находящийся на сцене
    public List<GameObject> _objectsOnSector = new List<GameObject>(); // Объекты находятся в данный момент на секторе (Юниты и т.д.)
    public HashSet<Sector> _neighborsSectors = new HashSet<Sector>(); // Соседнии сектора   - используется для поиска и прочее.
    public Sector[] _neighborsToMove;   // Соседнии сектора на которые можно пойти  - используется для поиска и прочее.

    //Для AI
    public List<Sector> _attackedNeighbotSectors = new List<Sector>(); // Сектора которые будут подвержены атаке при ударе в этот сектор
    public List<Sector> _sectorsToAttack = new List<Sector>(); // Сектора которые юнит может атоковать с этого сектора  (с этого сектора - атакуем)


    //public Dictionary<Unit, Sector> _hurtUnit = new Dictionary<Unit, Sector>();  //Кого задеваем Атакуя данный сектор
    //public Dictionary<Unit, Sector> _unitToAct = new Dictionary<Unit, Sector>();  //На кого можем напасть с данной клетки

    public Dictionary<Sector, Unit> _hurtUnit = new Dictionary<Sector, Unit>();  //Кого задеваем Атакуя данный сектор
    public Dictionary<Sector, Unit> _unitToAct = new Dictionary<Sector, Unit>();  //На кого можем напасть с данной клетки




   // public DataTable gg  = new DataTable();
  


    public int[] _data; // данные о transforme с карты

    // заблокированные направления хода у сектора
    public bool _dirX = false;
    public bool _dirY = false;
    public bool _dirZ = false;
    public bool _dirMinusX = false;
    public bool _dirMinusY = false;
    public bool _dirMinusZ = false;

    // Для просчёта пути 
    public int _chekedStage = 0; // Служебное поле Номер шага проверки (на каком ходу сектор доступен)
    public bool _cheked = false; // Служебное поле Номер шага проверки (на каком ходу сектор доступен)
    public Vector3 _previousArrayPosition; //На клетку пришли с клетки (Для поиска пути)

    // заблокированные направления Стеной у сектора
    public bool _wallDirX = false;
    public bool _wallDirY = false;
    public bool _wallDirZ = false;
    public bool _wallDirMinusX = false;
    public bool _wallDirMinusY = false;
    public bool _wallDirMinusZ = false;

    public Vector3 _onArrayPosition; //Позиция В массиве
}
