﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoCashed : MonoBehaviour   //  Листы апдейтов
{
    public static List<MonoCashed> _allTicks = new List<MonoCashed>();  //
    public static List<MonoCashed> _allFixedTicks = new List<MonoCashed>(); // Для физических объектов
    public static List<MonoCashed> _allLateTicks = new List<MonoCashed>();//
    private void OnEnable() // Добавляем объект в список для просчёта если он включен
    {
        if (this.GetType() == this.GetType().GetMethod("OnTick").DeclaringType) // проверяем наличие нужного меода у класса
            _allTicks.Add(this);
        if (this.GetType() == this.GetType().GetMethod("OnFixedTick").DeclaringType)
            _allFixedTicks.Add(this);
        if (this.GetType() == this.GetType().GetMethod("OnLateTick").DeclaringType)
            _allLateTicks.Add(this);
    }
    private void OnDisable()// Убераем объект из списока для просчёта если он включен
    {
        if (this.GetType() == this.GetType().GetMethod("OnTick").DeclaringType)
            _allTicks.Remove(this);
        if (this.GetType() == this.GetType().GetMethod("OnFixedTick").DeclaringType)
            _allFixedTicks.Remove(this);
        if (this.GetType() == this.GetType().GetMethod("OnLateTick").DeclaringType)
            _allLateTicks.Remove(this);
    }
    public void Tick()
    {
        OnTick();
    }
    public void FixedTick()
    {
        OnFixedTick();
    }
    public void LateTick()
    {
        OnLateTick();
    }
    public virtual void OnTick()   //  наш Update
    {
    }
    public virtual void OnFixedTick()   //  наш Fixed Update
    {
    }
    public virtual void OnLateTick()   //  наш Late Update
    {
    }
}