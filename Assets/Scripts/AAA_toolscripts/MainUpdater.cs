﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUpdater : MonoBehaviour     // Этот update для всех объектов в списке на обновление.  Списки в классе MonoCashed // Этот update един для всех. 
{
    bool _FixedTicksEverySecond;  // true - каждый  второй проход FixedUpdate

    private void Awake()
    {
        _FixedTicksEverySecond = false;
    }

    void Update()
    {
        for (var i = 0; i < MonoCashed._allTicks.Count; i++)
        {
            MonoCashed._allTicks[i].Tick();
        }
    }

    void FixedUpdate()
    {
        _FixedTicksEverySecond = !_FixedTicksEverySecond;
        for (var i = 0; i < MonoCashed._allFixedTicks.Count; i++)
        {
            if (MonoCashed._allFixedTicks[i].name == "Liquid")  // Если Update для жидкости
            {
                if (_FixedTicksEverySecond)  // Выполняем только каждый второй Update
                {
                    MonoCashed._allFixedTicks[i].FixedTick();
                }
            }
            else
            {
                MonoCashed._allFixedTicks[i].FixedTick();
            }
        }
    }

    void LateUpdate()
    {
        for (var i = 0; i < MonoCashed._allLateTicks.Count; i++)
        {
            MonoCashed._allLateTicks[i].LateTick();
        }
    }
}
