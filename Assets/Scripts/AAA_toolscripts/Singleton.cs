﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// public class Singleton<T> : MonoCashed where T : MonoCashed  // sigltone
public class Singleton<T> : MonoCashed where T : Singleton<T>   // sigltone
{
    public static bool _isApplicationQuitting; // для проверки  если выходим из приложения
    private static System.Object _lock = new System.Object(); // Для синхронизации с потоками (при вызове синглтона из паралельного потока)

    private static T _instance;  // sigltone
    public static T Instance
    {
        get
        {
            if (_isApplicationQuitting)  // если закрывается приложение - то  возвращаем - нул
                return null;

            lock (_lock)// Лочим кусок кода Пока не закончилась  обработка этого куска кода
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();//Пробуем найти на сцене

                    if (_instance == null)//проверяем - есть ли контроллер.
                    {
                        var singleton = new GameObject("[SINGLETON] " + typeof(T));
                        _instance = singleton.AddComponent<T>();
                        DontDestroyOnLoad(singleton);
                    }
                }
                return _instance;
            }
        }
    }


    public static T Initialize()  // Метод инициализации синглтона
    {
        return Instance;
    }

    public virtual void OnDestroy()
    {
        _isApplicationQuitting = true;
    }
}
