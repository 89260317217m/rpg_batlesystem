﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using plyLib;
using TileEd;

//using System.Runtime.CompilerServices.;

public class BattlefieldArray : Singleton<BattlefieldArray>   // 3д Массив поля боя (Временно и массив и построитель)
{
    public int _xMax;  // Ширина массива
    public int _yMax;  // Высота массива
    public int _zMax;  // Глубина массива

    int _xFrom = 0; // Края массив - для схождения массива карты и массива поля боя
    int _yFrom = 0; // Края массив - для схождения массива карты и массива поля боя
    int _zFrom = 0; // Края массив - для схождения массива карты и массива поля боя

    public Sector[,,] _array3D;   // Массив поля боя

    GameObject _goMapTileEd;      // Объект карты 
    TileEdMap _mapMy;
    public TileEdMapAsset _someInstance;  // Данные о карте


    private void OnEnable()
    {
        _goMapTileEd = GameObject.Find("TileEdMapLevel");
        _mapMy = _goMapTileEd.GetComponent<TileEdMap>();  // Смотрим какой ID  карты
        if (_someInstance == null)
        {
           // Debug.Log("Неприцепленная карта");
            //        someInstance = Resources.Load<TileEdMapAsset>("Maps\\" + mapMy.ident);  // получаем данные о карте
            _someInstance = Resources.Load<TileEdMapAsset>(_mapMy.ident);  // получаем данные о карте
        }
        System.DateTime startTime = System.DateTime.Now;
        Debug.Log("FillArray Time: " + startTime);
        FillArray();
        Debug.Log("FillArray Time: " + (startTime - System.DateTime.Now).TotalSeconds + "sec");
    }

    void FillArray()  // Заполняем массив данными из Карты 
    {
        // Определяем по карте размеры массива.
        _yMax = _someInstance.groups.Count - 1;
        foreach (TileEd.TileEdMapGroup group in _someInstance.groups)
        {
            if (Mathf.Abs(group.GetMinMaxGridPosition()[0].x) + Mathf.Abs(group.GetMinMaxGridPosition()[1].x) > _xMax)
            {
                _xMax = Mathf.Abs(group.GetMinMaxGridPosition()[0].x) + Mathf.Abs(group.GetMinMaxGridPosition()[1].x);
            }

            if (Mathf.Abs(group.GetMinMaxGridPosition()[0].z) + Mathf.Abs(group.GetMinMaxGridPosition()[1].z) > _zMax)
            {
                _zMax = Mathf.Abs(group.GetMinMaxGridPosition()[0].z) + Mathf.Abs(group.GetMinMaxGridPosition()[1].z);
            }
        }


        foreach (TileEd.TileEdMapGroup group in _someInstance.groups)  // Отцентруем запросы к картам по по самым большим минусам матрицы
        {
            if (group.GetMinMaxGridPosition()[0].x < _xFrom)
            {
                _xFrom = group.GetMinMaxGridPosition()[0].x;
            }
            if (group.GetMinMaxGridPosition()[0].y < _yFrom)
            {
                _yFrom = group.GetMinMaxGridPosition()[0].y;
            }
            if (group.GetMinMaxGridPosition()[0].z < _zFrom)
            {
                _zFrom = group.GetMinMaxGridPosition()[0].z;
            }
        }

        _array3D = new Sector[_xMax + 1, _yMax + 1, _zMax + 1];
        for (int y = 0; y < _array3D.GetLength(1); y++)
        {
            for (int x = 0; x < _array3D.GetLength(0); x++)
            {
                for (int z = 0; z < _array3D.GetLength(2); z++)
                {
                    IntVector3 iv = new IntVector3(x + _xFrom, y, z + _zFrom);
                    if (_someInstance.groups[y].GetTile(1, iv) != null)
                    {
                        // Попытаемся найти объект на сцене и взять компонент его параметров
                        GameObject TitleObj = GameObject.Find(_someInstance.groups[y].GetTile(1, iv).gridPos.ToString());
                        Sector sector = TitleObj.AddComponent<Sector>();
                        _array3D[x, y, z] = sector; //Добавляем в массив данные о секторе
                        _array3D[x, y, z]._meta = _someInstance.groups[y].GetTile(1, iv).meta;
                        _array3D[x, y, z]._onArrayPosition = new Vector3(x, y, z);
                        _array3D[x, y, z]._tileOnSector = TitleObj;
                        _array3D[x, y, z]._data = _someInstance.groups[y].GetTile(1, iv).data;
                        SetBlockedDirections(x, y, z, iv);  //Заносим в сектор данные о  заблокированных с него направлениях
                    }
                }
            }
        }
        SetNeighborsToSector();// Прописсываем в массиве сектора - его соседние сектора
    }

    public Vector3 ArrayToMapPosition (dynamic arX, dynamic arY, dynamic arZ)  // Координаты массива в позицию на карте
    {
        Vector3 onMapPosition = new Vector3();
        IntVector3 iv = new IntVector3((int)arX + _xFrom, (int)arY + _yFrom, (int)arZ + _zFrom);
        onMapPosition = new Vector3(_someInstance.groups[(int)arY].GetTile(1, iv).gridPos.x, _someInstance.groups[(int)arY].GetTile(1, iv).gridPos.y, _someInstance.groups[(int)arY].GetTile(1, iv).gridPos.z);
        return onMapPosition;
    }

    void SetBlockedDirections(int x, int y, int z, IntVector3 iv)  //Заносим в сектор данные о  заблокированных с него направлениях
    {
        if (_someInstance.groups[y].GetTile(1, iv).meta == "flor")  // на полу блокируем ход в низ
        {
            _array3D[x, y, z]._dirMinusY = true;
            _array3D[x, y, z]._wallDirMinusY = true;
        }

        if (_someInstance.groups[y].GetTile(1, iv).meta == "concave")  // на полу блокируем ход в низ
        {
            _array3D[x, y, z]._dirMinusY = true;
            _array3D[x, y, z]._wallDirMinusY = true;
        }
        if (_someInstance.groups[y].GetTile(1, iv).meta == "special")  // на полу блокируем ход в низ
        {
            _array3D[x, y, z]._dirMinusY = true;
            _array3D[x, y, z]._wallDirMinusY = true;
        }

        if (_someInstance.groups[y].GetTile(1, iv).meta == "ramp")  // Блокируем боковые направления
        {
            _array3D[x, y, z]._dirMinusY = true;
            _array3D[x, y, z]._wallDirMinusY = true;

            int inaccuracy = 30; // неточность
            if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 0) <= inaccuracy)
            {
                _array3D[x, y, z]._wallDirZ = true;
                _array3D[x, y, z]._dirZ = true;
                _array3D[x, y, z]._dirX = true;
                _array3D[x, y, z]._dirMinusX = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 90) <= inaccuracy)
            {
                _array3D[x, y, z]._wallDirX = true;
                _array3D[x, y, z]._dirX = true;
                _array3D[x, y, z]._dirZ = true;
                _array3D[x, y, z]._dirMinusZ = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 180) <= inaccuracy)
            {
                _array3D[x, y, z]._wallDirMinusZ = true;
                _array3D[x, y, z]._dirMinusZ = true;
                _array3D[x, y, z]._dirX = true;
                _array3D[x, y, z]._dirMinusX = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 270) <= inaccuracy)
            {

                _array3D[x, y, z]._wallDirMinusX = true;
                _array3D[x, y, z]._dirMinusX = true;
                _array3D[x, y, z]._dirMinusZ = true;
                _array3D[x, y, z]._dirZ = true;
            }
        }

        if (_someInstance.groups[y].GetTile(1, iv).meta == "wall") // Блокируем направления за стеной
        {
            _array3D[x, y, z]._dirMinusY = true;
            _array3D[x, y, z]._wallDirMinusY = true;

            int inaccuracy = 30; // неточность
            if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 0) <= inaccuracy)
            {
                _array3D[x, y, z]._dirZ = true;
                _array3D[x, y, z]._wallDirZ = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 90) <= inaccuracy)
            {
                _array3D[x, y, z]._dirX = true;
                _array3D[x, y, z]._wallDirX = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 180) <= inaccuracy)
            {
                _array3D[x, y, z]._dirMinusZ = true;
                _array3D[x, y, z]._wallDirMinusZ = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 270) <= inaccuracy)
            {
                _array3D[x, y, z]._dirMinusX = true;
                _array3D[x, y, z]._wallDirMinusX = true;
            }
        }

        if (_someInstance.groups[y].GetTile(1, iv).meta == "convex") // Блокируем направления за стенами
        {
            _array3D[x, y, z]._dirMinusY = true;
            _array3D[x, y, z]._wallDirMinusY = true;
            int inaccuracy = 30; // неточность
            if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 0) <= inaccuracy)
            {
                _array3D[x, y, z]._dirZ = true;
                _array3D[x, y, z]._dirX = true;
                _array3D[x, y, z]._wallDirZ = true;
                _array3D[x, y, z]._wallDirX = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 90) <= inaccuracy)
            {
                _array3D[x, y, z]._dirX = true;
                _array3D[x, y, z]._dirMinusZ = true;
                _array3D[x, y, z]._wallDirX = true;
                _array3D[x, y, z]._wallDirMinusZ = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 180) <= inaccuracy)
            {
                _array3D[x, y, z]._dirMinusZ = true;
                _array3D[x, y, z]._dirMinusX = true;
                _array3D[x, y, z]._wallDirMinusZ = true;
                _array3D[x, y, z]._wallDirMinusX = true;
            }
            else if (Mathf.Abs(_someInstance.groups[y].GetTile(1, iv).data[3] - 270) <= inaccuracy)
            {
                _array3D[x, y, z]._dirMinusX = true;
                _array3D[x, y, z]._dirZ = true;
                _array3D[x, y, z]._wallDirMinusX = true;
                _array3D[x, y, z]._wallDirZ = true;
            }
        }
    }

    void SetNeighborsToSector() // Прописсываем в массиве сектора - его соседние сектора
    {
        for (int x = 0; x < _array3D.GetLength(0); x++)
        {
            for (int y = 0; y < _array3D.GetLength(1); y++)
            {
                for (int z = 0; z < _array3D.GetLength(2); z++)
                {
                    if (_array3D[x, y, z] != null)  // Какой сектор проверяем
                    {
                        //  Куб ---------------------------------
                        //for (int xn = -1; xn < 1; xn++)  // Собираем всех соседей в кубе
                        //{
                        //    for (int yn = -1; yn < 1; yn++)
                        //    {
                        //        for (int zn = -1; zn < 1; zn++)
                        //        {
                        //            int newX = x + xn;
                        //            int newY = y + yn;
                        //            int newZ = z + zn;
                        //            if (newX < array3D.GetLength(0) & newX >= 0
                        //               & newY < array3D.GetLength(1) & newY >= 0
                        //               & newZ < array3D.GetLength(2) & newZ >= 0)
                        //            {
                        //                //if (array3D[newX, newY, newZ] != null & array3D[newX, newY, newZ] != array3D[x, y, z])
                        //                if (array3D[newX, newY, newZ] != null)
                        //                {
                        //                    array3D[x, y, z].neighborsSector.Add(array3D[newX, newY, newZ]);
                        //                }
                        //            }
                        //        }
                        //    }
                        //}


                        // крест   -----------------------------------------------------
                        int checkX;
                        int checkY;
                        int checkZ;

                        checkX = x - 1;
                        checkY = y - 1;
                        checkZ = z - 1;

                        if (checkX < _array3D.GetLength(0) & checkX >= 0)
                        {
                            if (_array3D[checkX, y, z] != null)
                                _array3D[x, y, z]._neighborsSectors.Add(_array3D[checkX, y, z]);
                        }
                        if (checkY < _array3D.GetLength(1) & checkY >= 0)
                        {
                            if (_array3D[x, checkY, z] != null)
                                _array3D[x, y, z]._neighborsSectors.Add(_array3D[x, checkY, z]);
                        }
                        if (checkZ < _array3D.GetLength(2) & checkZ >= 0)
                        {
                            if (_array3D[x, y, checkZ] != null)
                                _array3D[x, y, z]._neighborsSectors.Add(_array3D[x, y, checkZ]);
                        }

                        checkX = x + 1;
                        checkY = y + 1;
                        checkZ = z + 1;

                        if (checkX < _array3D.GetLength(0) & checkX >= 0)
                        {
                            if (_array3D[checkX, y, z] != null)
                                _array3D[x, y, z]._neighborsSectors.Add(_array3D[checkX, y, z]);
                        }
                        if (checkY < _array3D.GetLength(1) & checkY >= 0)
                        {
                            if (_array3D[x, checkY, z] != null)
                                _array3D[x, y, z]._neighborsSectors.Add(_array3D[x, checkY, z]);
                        }
                        if (checkZ < _array3D.GetLength(2) & checkZ >= 0)
                        {
                            if (_array3D[x, y, checkZ] != null)
                                _array3D[x, y, z]._neighborsSectors.Add(_array3D[x, y, checkZ]);
                        }

                        CheckUnusualNeighbors(_array3D[x, y, z]); // Проверяем нестандартных соседей.
                        CheckNeighborsToMove(_array3D[x, y, z]); // Добавляем  Соседей на которых можно зайти.
                    }
                }
            }
        }
    }

    void CheckNeighborsToMove(Sector chekedSector)
    {
        List<Sector> neighborSectorToMove = new List<Sector>();
        foreach (Sector neighborSector in chekedSector._neighborsSectors) // для каждого соседнего сектора
        {
            bool addToNeighbor = false;  // Доступен ли соседний сектор для хода на него

            int neighborPosX = (int)neighborSector._onArrayPosition.x;
            int neighborPosY = (int)neighborSector._onArrayPosition.y;
            int neighborPosZ = (int)neighborSector._onArrayPosition.z;

            int checkedPosX = (int)chekedSector._onArrayPosition.x;
            int checkedPosY = (int)chekedSector._onArrayPosition.y;
            int checkedPosZ = (int)chekedSector._onArrayPosition.z;

            //если ходим В одной плоскости
            bool onePlane = neighborPosY == checkedPosY;
            //если ходим В одной плоскости с рампы на рампу  и нет ли над рампами пола
            bool onePlaneTwoRamp = chekedSector._meta == "ramp" & neighborSector._meta == "ramp" & neighborPosY == checkedPosY
                & (_yMax >= checkedPosY + 1) ?
                ((_array3D[checkedPosX, checkedPosY + 1, checkedPosZ] != null) ? _array3D[checkedPosX, checkedPosY + 1, checkedPosZ]._wallDirMinusY != true : true) : false
                 & (_yMax >= neighborPosY + 1) ?
                ((_array3D[neighborPosX, neighborPosY + 1, neighborPosZ] != null) ? _array3D[neighborPosX, neighborPosY + 1, neighborPosZ]._wallDirMinusY != true : true) : false
                ;
            // если ходим с рампы
            bool chekedSectorIsRamp = chekedSector._meta == "ramp" && neighborPosY - 1 == checkedPosY
                  & (_yMax >= checkedPosY + 1) ?
                ((_array3D[checkedPosX, checkedPosY + 1, checkedPosZ] != null) ? _array3D[checkedPosX, checkedPosY + 1, checkedPosZ]._wallDirMinusY != true : true) : false
                ;
            // Если ходим на рампу с верху
            bool neighborSectorIsRamp = neighborSector._meta == "ramp" && neighborPosY + 1 == checkedPosY;

            if (onePlane & chekedSector._dirX == false & neighborSector._dirMinusX == false & checkedPosX + 1 == neighborPosX) // Если разрешено смещение в верх по Х и сосед верхний 
            {
                addToNeighbor = true;
            }
            else if (onePlane & chekedSector._dirMinusX == false & neighborSector._dirX == false & checkedPosX - 1 == neighborPosX) // Если разрешено смещение  down по Х и сосед down 
            {
                addToNeighbor = true;
            }
            else if (onePlane & chekedSector._dirZ == false & neighborSector._dirMinusZ == false & checkedPosZ + 1 == neighborPosZ)
            {
                addToNeighbor = true;
            }
            else if (onePlane & chekedSector._dirMinusZ == false & neighborSector._dirZ == false & checkedPosZ - 1 == neighborPosZ)
            {
                addToNeighbor = true;
            }

            else if (onePlaneTwoRamp & (chekedSector._data[3] == neighborSector._data[3] | Mathf.Abs(Mathf.Abs(chekedSector._data[3]) - Mathf.Abs(neighborSector._data[3])) == 180))
            {
                addToNeighbor = true;
            }

            //если ходим В верх - в низ
            else if (chekedSector._dirY == false & neighborSector._dirMinusY == false & checkedPosY + 1 == neighborPosY)
            {
                addToNeighbor = true;
            }
            else if (chekedSector._dirMinusY == false & neighborSector._dirY == false & checkedPosY - 1 == neighborPosY)
            {
                addToNeighbor = true;
            }

            else if (chekedSectorIsRamp == true & neighborSector._dirMinusX == false & checkedPosX + 1 == neighborPosX) // Если разрешено смещение в верх по Х и сосед верхний 
            {
                addToNeighbor = true;
            }
            else if (chekedSectorIsRamp == true & neighborSector._dirX == false & checkedPosX - 1 == neighborPosX) // Если разрешено смещение в верх по Х и сосед верхний 
            {
                addToNeighbor = true;
            }
            else if (chekedSectorIsRamp == true & neighborSector._dirMinusZ == false & checkedPosZ + 1 == neighborPosZ) // Если разрешено смещение в верх по Z и сосед верхний 
            {
                addToNeighbor = true;
            }
            else if (chekedSectorIsRamp == true & neighborSector._dirZ == false & checkedPosZ - 1 == neighborPosZ) // Если разрешено смещение в верх по Z и сосед верхний 
            {
                addToNeighbor = true;
            }

            else if (neighborSectorIsRamp & chekedSector._dirX == false & neighborSector._dirMinusX == true & neighborSector._wallDirMinusX == true & checkedPosX + 1 == neighborPosX) // Если разрешено смещение в верх по Х и сосед верхний 
            {
                addToNeighbor = true;
            }
            else if (neighborSectorIsRamp & chekedSector._dirMinusX == false & neighborSector._dirX == true & neighborSector._wallDirX == true & checkedPosX - 1 == neighborPosX) // Если разрешено смещение  down по Х и сосед down 
            {
                addToNeighbor = true;
            }

            else if (neighborSectorIsRamp & chekedSector._dirZ == false & neighborSector._dirMinusZ == true & neighborSector._wallDirMinusZ == true & checkedPosZ + 1 == neighborPosZ)
            {
                addToNeighbor = true;
            }
            else if (neighborSectorIsRamp & chekedSector._dirMinusZ == false & neighborSector._dirZ == true & neighborSector._wallDirZ == true & checkedPosZ - 1 == neighborPosZ)
            {
                addToNeighbor = true;
            }
            else // Если на соседний сектор не зайти
            {
                // neighborSector.chekedStage = 0;
            }

            if (addToNeighbor) // Если сектор подходит - добавляем его
            {
                neighborSectorToMove.Add(neighborSector);
            }
        }

        if (neighborSectorToMove.Count > 0)
        {
            chekedSector._neighborsToMove = neighborSectorToMove.ToArray();  // Добавляем в сектор список секторов на который можно пойти
        }

    }

    void CheckUnusualNeighbors(Sector chekedSector) // Проверяем нестандартных соседей Типа рампы для подъёма или спуска.
    {
        if (chekedSector._meta == "ramp") // Если проверяемый сектор - рампа, добавляем ему в соседи верхний со смещением по направлению рампы.
        {
            int cx = (int)chekedSector._onArrayPosition.x; // Новая позиция со смещением от рампы.
            int cy = (int)chekedSector._onArrayPosition.y;// Новая позиция со смещением от рампы.
            int cz = (int)chekedSector._onArrayPosition.z;// Новая позиция со смещением от рампы.
            int inaccuracy = 30; // неточность
            if (Mathf.Abs(chekedSector._data[3] - 0) <= inaccuracy)
            {
                cy = cy + 1; cz = cz + 1;
            }
            else if (Mathf.Abs(chekedSector._data[3] - 90) <= inaccuracy)
            {
                cy = cy + 1; cx = cx + 1;
            }
            else if (Mathf.Abs(chekedSector._data[3] - 180) <= inaccuracy)
            {
                cy = cy + 1; cz = cz - 1;
            }
            else if (Mathf.Abs(chekedSector._data[3] - 270) <= inaccuracy)
            {
                cy = cy + 1; cx = cx - 1;
            }
            if (cx < _array3D.GetLength(0) & cy < _array3D.GetLength(1) & cz < _array3D.GetLength(2))
            {
                if (_array3D[cx, cy, cz] != null)
                {
                    if (!chekedSector._neighborsSectors.Contains(_array3D[cx, cy, cz]))// Проверяем - не добавляли ли ранее
                        chekedSector._neighborsSectors.Add(_array3D[cx, cy, cz]);
                }
            }
        }
        // Проверяем нет ли под соседней пустой клеткой рампы.
        int cpx = (int)chekedSector._onArrayPosition.x; // Новая позиция со смещением.
        int cpy = (int)chekedSector._onArrayPosition.y;// Новая позиция со смещением.
        int cpz = (int)chekedSector._onArrayPosition.z;// Новая позиция со смещением.
        if (cpx + 1 < _array3D.GetLength(0) & cpy - 1 < _array3D.GetLength(1) & cpx + 1 >= 0 & cpy - 1 >= 0 & cpz >= 0)
        {
            if (_array3D[cpx + 1, cpy, cpz] == null & _array3D[cpx + 1, cpy - 1, cpz] != null) // Соседняя пустая клетка и под ней есть что-то
            {
                if (_array3D[cpx + 1, cpy - 1, cpz]._meta == "ramp")
                {

                    if (!chekedSector._neighborsSectors.Contains(_array3D[cpx + 1, cpy - 1, cpz]))  // Проверяем - не добавляли ли ранее
                        chekedSector._neighborsSectors.Add(_array3D[cpx + 1, cpy - 1, cpz]);
                }
            }
        }
        if (cpx - 1 < _array3D.GetLength(0) & cpy - 1 < _array3D.GetLength(1) & cpx - 1 >= 0 & cpy - 1 >= 0 & cpz >= 0)
        {
            if (_array3D[cpx - 1, cpy, cpz] == null & _array3D[cpx - 1, cpy - 1, cpz] != null) // Соседняя пустая клетка и под ней есть что-то
            {
                if (_array3D[cpx - 1, cpy - 1, cpz]._meta == "ramp")
                {
                    if (!chekedSector._neighborsSectors.Contains(_array3D[cpx - 1, cpy - 1, cpz]))  // Проверяем - не добавляли ли ранее
                        chekedSector._neighborsSectors.Add(_array3D[cpx - 1, cpy - 1, cpz]);
                }
            }
        }
        if (cpz + 1 < _array3D.GetLength(2) & cpy - 1 < _array3D.GetLength(1) & cpx >= 0 & cpy - 1 >= 0 & cpz + 1 >= 0)
        {
            if (_array3D[cpx, cpy, cpz + 1] == null & _array3D[cpx, cpy - 1, cpz + 1] != null) // Соседняя пустая клетка и под ней есть что-то
            {
                if (_array3D[cpx, cpy - 1, cpz + 1]._meta == "ramp")
                {
                    if (!chekedSector._neighborsSectors.Contains(_array3D[cpx, cpy - 1, cpz + 1]))  // Проверяем - не добавляли ли ранее
                        chekedSector._neighborsSectors.Add(_array3D[cpx, cpy - 1, cpz + 1]);
                }
            }
        }
        if (cpz - 1 < _array3D.GetLength(2) & cpy - 1 < _array3D.GetLength(1) & cpx >= 0 & cpy - 1 >= 0 & cpz - 1 >= 0)
        {
            if (_array3D[cpx, cpy, cpz - 1] == null & _array3D[cpx, cpy - 1, cpz - 1] != null) // Соседняя пустая клетка и под ней есть что-то
            {
                if (_array3D[cpx, cpy - 1, cpz - 1]._meta == "ramp")
                {
                    if (!chekedSector._neighborsSectors.Contains(_array3D[cpx, cpy - 1, cpz - 1]))  // Проверяем - не добавляли ли ранее
                        chekedSector._neighborsSectors.Add(_array3D[cpx, cpy - 1, cpz - 1]);
                }
            }
        }
    }

    public void StartPreRespUnit() // На старте Респит юниты в местах указанных на карте
    {
        for (int y = 0; y <_array3D.GetLength(1); y++)
        {
            for (int x = 0; x < _array3D.GetLength(0); x++)
            {
                for (int z = 0; z <_array3D.GetLength(2); z++)
                {
                    IntVector3 iv = new IntVector3(x + _xFrom, y, z + _zFrom);
                    if (_array3D[x, y, z] != null)
                    {
                        if (_array3D[x, y, z]._meta == "enemyResp" | _array3D[x, y, z]._meta == "playerResp")  // Респим юнита на камере для теста
                        {
                            int xMap = _someInstance.groups[y].GetTile(1, iv).gridPos.x;
                            int yMap = _someInstance.groups[y].GetTile(1, iv).gridPos.y;
                            int zMap = _someInstance.groups[y].GetTile(1, iv).gridPos.z;
                            RespUnitsOnMap(xMap, yMap, zMap, new Vector3(x, y, z), _array3D[x, y, z]._meta);
                        }
                    }
                }
            }
        }
    }

    void RespUnitsOnMap(int xMap, int yMap, int zMap, Vector3 inBattleArray, string unitType)   //  Респим  unit
    {
        GameObject obj = (GameObject)Resources.Load("Prefab/" + "Unit", typeof(GameObject));
        Vector3 posPrefab = obj.transform.position;
        GameObject instance = Instantiate(obj) as GameObject;
        instance.transform.position = new Vector3(xMap, yMap, zMap);
        instance.GetComponent<Unit>()._posOnArray = inBattleArray; // Добавляем данные о нахождении в матрицк
        instance.GetComponent<Unit>()._unitOnSector = _array3D[(int)inBattleArray.x, (int)inBattleArray.y, (int)inBattleArray.z]; // Добавляем ссылку на сектор в котором юнит

        if (unitType == "playerResp") // Красим юнита
        {
            instance.transform.GetChild(0).GetComponent<Renderer>().material = Resources.Load("Material/BlueMat", typeof(Material)) as Material;
            instance.transform.tag = "unitPlayer";
            instance.transform.GetComponent<Unit>()._playerUnit = TurnManager.Instance.Players.Find(x => x._name == "player");
            TurnManager.Instance.Players.Find(x => x._name == "player")._playerUnits.Add(instance);

        }
        else if (unitType == "enemyResp")
        {
            instance.transform.GetChild(0).GetComponent<Renderer>().material = Resources.Load("Material/RedMat", typeof(Material)) as Material;
            instance.transform.tag = "unitEnemy";
            instance.transform.GetComponent<Unit>()._playerUnit = TurnManager.Instance.Players.Find(x => x._name == "enemy");
            TurnManager.Instance.Players.Find(x => x._name == "enemy")._playerUnits.Add(instance);
        }
        _array3D[(int)inBattleArray.x, (int)inBattleArray.y, (int)inBattleArray.z]._objectsOnSector.Add(instance); // Добавляем в сектор массив BattlefieldArray данные о том что в нём находится юнит
    }
}