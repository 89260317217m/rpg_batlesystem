﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using System.Data;
using System.Linq;
using System;

public class TurnManager : Singleton<TurnManager>   // Хранит глобальные переменные   На поле боя и управляет очерёдностью ходов
{
    int _turnCount; public int TurnCount //  Ходов прошло
    { get { return _turnCount; } set { _turnCount = value; } }
    List<Player> _players = new List<Player>(); public List<Player> Players //   Лист игроков
    { get { return _players; } set { _players = value; } }
    Player _currentPlayer; public Player CurrentPlayer //  текущий игрок
    { get { return _currentPlayer; } set { _currentPlayer = value; } }


    private void Awake()
    {
        EventSys.Instance._endTour += NextPlayer;// подписываемся на событие Смены тура

        Player player = new Player("player", true);
        Player comp1 = new Player("enemy", false);
        Players.Add(player);
        Players.Add(comp1);
        CurrentPlayer = Players[0];
        BattlefieldArray.Instance.StartPreRespUnit();  // Раставляем юниты на карте
        PlayerUnitsEnableDesable(CurrentPlayer, false);

    }

    public void NextPlayer()   // Смена хода
    {
        PlayerUnitsEnableDesable(CurrentPlayer, true);
        Debug.Log("Переход хода");
        int indexNextPlayer = Players.IndexOf(CurrentPlayer) + 1;
        Debug.Log("Игрок до смены тура:" + Players.IndexOf(CurrentPlayer));
        if (indexNextPlayer >= Players.Count())
        { indexNextPlayer = 0; }
        CurrentPlayer = Players[indexNextPlayer];
        PlayerUnitsEnableDesable(CurrentPlayer, false);
        Debug.Log("Игрок ПОСЛЕ смены тура:" + Players.IndexOf(CurrentPlayer));

        if (CurrentPlayer._name == "enemy")
        {
            GameObject.Find("[CONTROLLERS]").GetComponent<ControllerAI>().StartTurn();

        }

    }

    public void PlayerUnitsEnableDesable(Player player, bool endTurn) // Активация или деакцивация всех юнитов игрока
    {
        CurrentPlayer._playerUnits.ForEach(u => u.GetComponent<Unit>().EndTurn = endTurn);
    }

}