﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;

public class WorkWithDB : Singleton<WorkWithDB>
{
    //private DataTable MyTableData { get; set; }  // таблица в которую грузим данные из Базы данных.
    //private SqliteConnection Connect(string dbName)  // Конектимся к указанной базе.
    //{
    //    string pathDB = "";
    //    SqliteConnection connection = null;
    //    MyTableData = new DataTable();
    //    try
    //    {
    //        if (Application.platform != RuntimePlatform.Android)
    //        {
    //            pathDB = Application.dataPath + "/StreamingAssets/" + dbName; // путь для виндовс
    //        }
    //        if (Application.platform == RuntimePlatform.Android)
    //        {
    //            pathDB = Application.persistentDataPath + "/" + dbName; // путь для андройда
    //            if (!File.Exists(pathDB)) // Если файла нету - записываем его на устройство из пакета
    //            {
    //                WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + dbName);
    //                while (!loadDB.isDone) { }
    //                try
    //                {
    //                    File.WriteAllBytes(pathDB, loadDB.bytes);
    //                }
    //                catch (Exception ex)
    //                {
    //                    Debug.Log("Error1!!! " + ex.ToString());
    //                }
    //            }
    //        }
    //        connection = new SqliteConnection("URI=file:" + pathDB);
    //        connection.Open();
    //    }
    //    catch (Exception ex)
    //    {
    //        Debug.Log("Error2!!! " + ex.ToString());
    //    }
    //    return connection;
    //}

    //private void Disconnect(SqliteConnection connection)
    //{
    //    connection.Close();
    //    connection = null;
    //}

    //public DataTable LoadDataFromDB(string sqlQuery, string dbNameVar)  // Принимает в себя запрос и возвращает таблицу с данными от запроса
    //{
    //    SqliteConnection connection;
    //    connection = Connect(dbNameVar);
    //    try
    //    {
    //        using (connection)
    //        {
    //            using (SqliteCommand cmd = new SqliteCommand(sqlQuery, connection))
    //            {
    //                FillDatatable(cmd, MyTableData);
    //                Disconnect(connection);
    //                return MyTableData;   // возвращает таблицу с данными от запроса
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Debug.Log("Error!!! " + ex.ToString());
    //    }
    //    Disconnect(connection);
    //    return null; // возвращает таблицу = null  Если всё обломалось
    //}


    //public void InsertUpdate(string sqlQuery, string dbNameVar)  // Выполняем запросы к базе - вставка или обновление
    //{
    //    SqliteConnection connection;
    //    connection = Connect(dbNameVar);
    //    try
    //    {
    //        using (connection)
    //        {
    //            using (SqliteCommand cmd = new SqliteCommand(sqlQuery, connection))
    //            {
    //                cmd.ExecuteNonQuery();
    //                //   Disconnect(connection);
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Debug.Log("Error!!! " + ex.ToString());
    //    }
    //    Disconnect(connection);
    //}

    //private void FillDatatable(SqliteCommand command, DataTable dt) // Заполняет таблицу Данными из запроса SQL
    //{
    //    var reader = command.ExecuteReader();
    //    var len = reader.FieldCount;
    //    // Create the DataTable columns
    //    for (int i = 0; i < len; i++)
    //        dt.Columns.Add(reader.GetName(i), reader.GetFieldType(i));

    //    dt.BeginLoadData();
    //    var values = new object[len];
    //    // Add data rows
    //    while (reader.Read())
    //    {
    //        for (int i = 0; i < len; i++)
    //            values[i] = reader[i];
    //        dt.Rows.Add(values);
    //    }
    //    dt.EndLoadData();
    //    reader.Close();
    //    reader.Dispose();
    //}

    ////private Dictionary<Type, object> data = new Dictionary<Type, object>();
    ////public static T Get<T>()
    ////{
    ////    object resolve;
    ////    Instance.data.TryGetValue(typeof(T), out resolve);
    ////    Debug.Log("jjjj");
    ////    return (T)resolve;
    ////}

}