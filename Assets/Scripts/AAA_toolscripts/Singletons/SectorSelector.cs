﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Объявляем тип делегата
public delegate List<Sector> DelegatSelectAttack(int x, int y, int z, int pointAction);  // Делегат для метода  выбора клеток для атаки

public class SectorSelector : Singleton<SectorSelector>  // Поиск путей, выделение секторов, Поиск доступных клеток и т.д.
{
    public List<Sector> _selectedSectors = new List<Sector>();  // сектора в которые Выделили для хода
    public GameObject _selectedUnit = null;// Выбранный объект - юнит
    public Sector _selectedSector = null; // Выбранный сектор на карте

    public void GetStartPosition(GameObject unit, int pointAction = 0)  // Принимаем  объект начала для действия и выеляет возможный путь  - Юнит, его позиция и т.д.
    {
        ClearMarkSector(_selectedSectors);

        Vector3 position = unit.GetComponent<Unit>()._posOnArray;
        if (pointAction == 0)
        {
            pointAction = unit.GetComponent<Unit>().PointsAction;
        }
        int distanseAttack = unit.GetComponent<Unit>()._pointsDistanceAttack;
        int zoneAttack = unit.GetComponent<Unit>()._pointsZoneAttack;

        int x = (int)position.x;  //Точка в матрице от которой выделяем. В цент стороны ромб и т.д.
        int y = (int)position.y;//Точка в матрице от которой выделяем. В цент стороны ромб и т.д.
        int z = (int)position.z;//Точка в матрице от которой выделяем. В цент стороны ромб и т.д.

        DelegatSelectAttack delegatSelectAvailablePath = new DelegatSelectAttack(GetAvailablePath); // Создаём делегат на метод
        DelegatSelectAttack delegatSelectRomb = new DelegatSelectAttack(SelectRomb); // Создаём делегат на метод
        DelegatSelectAttack delegatSelectSector = new DelegatSelectAttack(SelectSector); // Создаём делегат на метод

        //  System.DateTime startTime = System.DateTime.Now;
        //  Debug.Log("GetAvailablePath to MOVE : " + startTime);

        //   _selectedSectors = GetAvailablePath(x, y, z, pointAction);

        _selectedSectors = FiandAvalibleInteractions(x, y, z, pointAction, distanseAttack, zoneAttack, delegatSelectAvailablePath, delegatSelectRomb, delegatSelectSector);

        //  Debug.Log("End to MOVE: " + (System.DateTime.Now - startTime).TotalSeconds + "sec");

        ColoredAllSelectedSector(_selectedSectors);
    }

    void ColoredAllSelectedSector(List<Sector> selectedSectors)  // Принимаем  список секторов на выделение
    {
        foreach (var item in selectedSectors)
        {
            SetColorOnSector((int)item._onArrayPosition.x, (int)item._onArrayPosition.y, (int)item._onArrayPosition.z, "0000FF");
        }
    }

    public List<Sector> GetAvailablePath(int x, int y, int z, int pointAction)  // Возвращает все клетки на которые можно зайти за Н ходов с этой клетки
    {
        HashSet<Sector> сheckedNeighbors = new HashSet<Sector>(); // Сектора для проверки в следующем шагу
        HashSet<Sector> openSector = new HashSet<Sector>(); // Сектора для проверки

        //ClearMarkSector();

        List<Sector> selectedSectors = new List<Sector>();
        int chekedStage = 0; // Начальный шаг
        Sector chekedSector = BattlefieldArray.Instance._array3D[x, y, z];
        openSector.Add(chekedSector);

        while (openSector.Count > 0 & chekedStage <= pointAction) // Пока в списке есть сектора для проверки - проверяем их
        {
            foreach (var item in openSector)
            {
                chekedSector = item; // Какой сектор проверяем
                for (int n = 0; n < chekedSector._neighborsToMove.Length; n++)
                {
                    Sector neighSec = chekedSector._neighborsToMove[n];
                    if (!neighSec._cheked)  // Проверяем - не проходили ли ранее
                    {
                        bool blockedLend = false;
                        if (neighSec._objectsOnSector != null)
                        {
                            foreach (var objOnSector in neighSec._objectsOnSector)
                            {
                                blockedLend = true;
                                if (!objOnSector.tag.Contains("unit"))
                                { blockedLend = true; }
                            }
                        }

                        if (!blockedLend)  // Если сектор не заблокирован юнитом или не завален её чём нить
                        {
                            neighSec._chekedStage = (chekedStage + 1);
                            neighSec._previousArrayPosition = chekedSector._onArrayPosition;  // Добавляем  сектор c С которого пришли
                            сheckedNeighbors.Add(neighSec);
                        }
                    }
                }
                chekedSector._cheked = true;
                selectedSectors.Add(chekedSector); // Добавляем в пройденные сектора
            }
            openSector.Clear(); // Очищаем список секторов которые только что проверели

            foreach (var item in сheckedNeighbors)
            {
                if (chekedStage == pointAction)  // Если не успеваем зайти на сектор (можем но ходы кончились) - Убераем с сектора метку что он доступен по ходу
                {
                    item._chekedStage = 0;
                }
                else
                {
                    openSector.Add(item);  // Добовляем в список следующего прохода сектора соседей
                }
            }
            сheckedNeighbors.Clear();
            chekedStage++; // Считаем шаги по клеткам.
        }


        return selectedSectors;
    }

    public List<Sector> SelectRomb(int x, int y, int z, int pointAction) // Выделяем ромб от центра
    {
        List<Sector> selectedSectors = new List<Sector>();
        for (int i = 0; i <= pointAction; i++)
        {
            foreach (Sector sector in SelectXLineOnCenter(x, y, z + i, pointAction - i))
            {
                selectedSectors.Add(sector);
            }

            foreach (Sector sector in SelectXLineOnCenter(x, y, z - i, pointAction - i))
            {
                selectedSectors.Add(sector);
            }
        }
        return selectedSectors;
    }

    public List<Sector> SelectSector(int x, int y, int z, int pointAction = 0) // Выделяем один сектор
    {
        List<Sector> selectedSectors = new List<Sector>();

        selectedSectors.Add(BattlefieldArray.Instance._array3D[x, y, z]);

        return selectedSectors;
    }

    public List<Sector> SelectXLineOnCenter(int x, int y, int z, int pointAction)  // Выделяем линии от центра В стороны Х
    {
        List<Sector> selectedSectors = new List<Sector>();
        for (int i = 0; i <= pointAction; i++)
        {
            if (SectorInMantrixRange(x + i, y, z))
            {
                selectedSectors.Add(BattlefieldArray.Instance._array3D[x + i, y, z]);
            }

            if (SectorInMantrixRange(x - i, y, z))
            {
                selectedSectors.Add(BattlefieldArray.Instance._array3D[x - i, y, z]);
            }
        }
        return selectedSectors;
    }

    public List<Sector> SelectYLineOnCenter(int x, int y, int z, int pointAction)  // Выделяем линии от центра В стороны Y
    {
        List<Sector> selectedSectors = new List<Sector>();
        for (int i = 0; i <= pointAction; i++)
        {
            if (SectorInMantrixRange(x, y + i, z))
            {
                selectedSectors.Add(BattlefieldArray.Instance._array3D[x, y + i, z]);
            }

            if (SectorInMantrixRange(x, y - i, z))
            {
                selectedSectors.Add(BattlefieldArray.Instance._array3D[x, y - i, z]);
            }
        }
        return selectedSectors;
    }

    bool SectorInMantrixRange(int x, int y, int z)  // Проверяем  что координаты в размерах матрицы и по ним есть сектор 
    {
        bool inrange = false;
        if (x < BattlefieldArray.Instance._array3D.GetLength(0) & y < BattlefieldArray.Instance._array3D.GetLength(1) & z < BattlefieldArray.Instance._array3D.GetLength(2) & x >= 0 & y >= 0 & z >= 0)  // Проверяем что сектор - в границах матрице
        {
            if (BattlefieldArray.Instance._array3D[x, y, z] != null)  // Если сектор который должен попасть в выделение не пустой
            {
                inrange = true;
                // SetColorOnSector(x, y, z, "0000FF");
            }
        }
        return inrange;
    }

    void SetColorOnSector(int x, int y, int z, string colorHEX = "FFFFFF") // Включаем(Красим) подсветку сектора
    {
        Transform child = BattlefieldArray.Instance._array3D[x, y, z]._tileOnSector.transform.Find("tileMark");
        child.gameObject.SetActive(true); // Включаем подсветку сектора
    }

    public void ClearMarkSector(List<Sector> sectors = null) // Снимаем метку с выделенных секторов
    {
        if (sectors == null)
        {
            foreach (Sector item in BattlefieldArray.Instance._array3D) // Удаляем данные с секторов о предыдущих проверках
            {
                if (item != null)
                {
                    item._unitToAct.Clear();
                    item._hurtUnit.Clear();
                    item._chekedStage = 0;
                    item._cheked = false;
                    item._attackedNeighbotSectors.Clear();
                    item._sectorsToAttack.Clear();
                    item._tileOnSector.transform.Find("tileMark").gameObject.SetActive(false); // Выключаем подсветку сектора
                }
            }
        }
        else
        {
            foreach (Sector item in sectors) // Удаляем данные с секторов о предыдущих проверках
            {
                if (item != null)
                {
                    item._unitToAct.Clear();
                    item._chekedStage = 0;
                    item._cheked = false;
                    item._attackedNeighbotSectors.Clear();
                    item._sectorsToAttack.Clear();
                    item._tileOnSector.transform.Find("tileMark").gameObject.SetActive(false); // Выключаем подсветку сектора
                }
            }
            sectors.Clear();
        }

        //foreach (var item in _selectedSectors)
        //{
        //    item._tileOnSector.transform.Find("tileMark").gameObject.SetActive(false); // Выключаем подсветку сектора
        //    item._chekedStage = 0;
        //    item._cheked = false;
        //    item._sectorsToAttack.Clear();
        //    item._attackedSectors.Clear();
        //}
        // _selectedSectors.Clear();
    }

    public List<Sector> MakePathToMove(Sector startSector, Sector endSector)  // Строим путь для перемещения  от сектора до сектора  включительно стартовый и конечный сектор 
    {
        List<Sector> path = new List<Sector>();
        int countMove = endSector._chekedStage;
        Sector checkedSector = endSector;
        path.Add(checkedSector);
        while (countMove != 0)
        {
            checkedSector = BattlefieldArray.Instance._array3D[(int)checkedSector._previousArrayPosition.x, (int)checkedSector._previousArrayPosition.y, (int)checkedSector._previousArrayPosition.z];  // Добавляем придыдущий сектор
            path.Add(checkedSector);
            countMove = checkedSector._chekedStage;
        }
        return path;
    }

    public List<Sector> FiandAvalibleInteractions(int x, int y, int z, int MovePoint, int distanseAttack, int zoneAttack, DelegatSelectAttack delegatFirstSelect, DelegatSelectAttack delegatSecondSelect, DelegatSelectAttack delegatThirdSelect)  // Поиск возможных взаимодействий с другими юнитами и предметами  С клетки
    {
        ClearMarkSector();// Удаляем данные с секторов о предыдущих проверках
        List<Sector> SectorsInteraction = new List<Sector>();  //  Сектора с которых можем взаимодействовать
        List<Sector> Select1 = new List<Sector>();  //  куда "можем дойти"
        Select1 = delegatFirstSelect(x, y, z, MovePoint);
        foreach (Sector sectorSelect1 in Select1)
        {
            List<Sector> Select2 = new List<Sector>();  //  клетки "можем атаковать"
            Select2 = delegatSecondSelect((int)sectorSelect1._onArrayPosition.x, (int)sectorSelect1._onArrayPosition.y, (int)sectorSelect1._onArrayPosition.z, distanseAttack);
            foreach (Sector sectorSelect2 in Select2)
            {
                List<Sector> Select3 = new List<Sector>();  //  клетки "подвержены атаке"
                Select3 = delegatSecondSelect((int)sectorSelect2._onArrayPosition.x, (int)sectorSelect2._onArrayPosition.y, (int)sectorSelect2._onArrayPosition.z, zoneAttack);
                foreach (Sector sectorSelect3 in Select3)
                {
                    if (sectorSelect3._objectsOnSector.Count != 0 & sectorSelect2._attackedNeighbotSectors.Contains(sectorSelect3) != true) // Если в секторе попадающем под действие атаки что-то есть
                    {
                        sectorSelect2._attackedNeighbotSectors.Add(sectorSelect3);

                        foreach (GameObject item in sectorSelect3._objectsOnSector) // Все объекты которые атакуем нв секторе 3-его селекта  записываем 
                        {
                            sectorSelect2._hurtUnit.Add(sectorSelect1, item.GetComponent<Unit>()); //[хз
                        }

                    }
                }
                if (sectorSelect2._attackedNeighbotSectors.Count != 0 & sectorSelect1._sectorsToAttack.Contains(sectorSelect2) != true) // Если атакуя этот сектор мы кого нить достанем
                {
                    sectorSelect1._sectorsToAttack.Add(sectorSelect2);

                    foreach (var item in sectorSelect2._hurtUnit) // Все объекты которые атакуем в секторе 3-его селекта  записываем 
                    {
                        sectorSelect1._unitToAct.Add(item.Key, item.Value);
                    }

                }
            }

            // Подщитываем ценность атаки именно с этого сектора
            if (sectorSelect1._sectorsToAttack.Count != 0 & SectorsInteraction.Contains(sectorSelect1) != true)
            {
                SectorsInteraction.Add(sectorSelect1);

            }
        }
        return SectorsInteraction;
    }


    public void StartMoveUnitByPath(List<Sector> path, GameObject objUnit) // Отправляем юнита по пути
    {
        foreach (Sector sect in path)  // Передаём юниту его путь по карте
        {
            objUnit.GetComponent<Unit>()._pathToMoveOnArray.Add(sect._onArrayPosition);
        }
        objUnit.GetComponent<Unit>()._unitIsMoving = true; // Говорим юниту что он перемещается.
    }



    // Временное решение
    public static Color HexToColor(string hex)  // Для перевода HEX  в RGB
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }
}


// (Select 1) Для каждой клетки куда "можем дойти" проверяем

// (Select 2) какие клетки с неё достанем  - "можем атаковать"  

// (Select 3) Какие другие клетки будут "подвержены атаке"

// Всё клетки "подверженые атаке" - записываем в лист клетки из (Select 2  - "можем атаковать") 

// Все клетки  из (Select 2  - "можем атаковать")  записываем в лист клетки  из (Select 1 - "можем дойти") 

// Для каждой клетки из (Select 1) в которой есть записи  -  Определяем цену ??

// 1) Цена - дистанция до клетки
// 2) Количество врагов.друзей на которых можно воздействовать  (приоритет много или мало - избегаем боя или наоборот)
// 3) приоритет воздействие (слабый, сильный или избранный)
