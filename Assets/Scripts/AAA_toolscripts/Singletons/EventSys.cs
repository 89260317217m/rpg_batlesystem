﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;


public class EventSys : Singleton<EventSys>  // Служба событий
{
    public delegate void EventChangeUnitParam(GameObject toUnit, string paramName, int addValue); // Делегат передачи сообщения
    public event EventChangeUnitParam _changeUnitParam;  // Событие

    public void SendEvent(GameObject toUnit, string paramName, int addValue)   // Изменить параметр для Юнита
    {
        _changeUnitParam?.Invoke(toUnit, paramName, addValue); // Если есть подпищики - cоздаём событие  (Вызываем делегат)
    }

    public delegate void EventEndTour(); // Делегат передачи сообщения  конца тура
    public event EventEndTour _endTour;  // Событие конца тура

    public void SendEventEndTour()   // Инициализируем событие(о конце тура) для всех подписчиков 
    {
        _endTour?.Invoke(); // Если есть подпищики - Запускаем методы подписавшихся
    }
}




// public event Action OnCount = delegate { };    ИЗУЧИТЬ ДЕЛЕГАТЫ!!