﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using UnityEditor;
using UnityEngine.SceneManagement;

public class Starter : MonoBehaviour // Для инициализации менеджеров
{
    public List<string> _singles = new List<string>();  // Список менеджеров синглтонов
    void Awake()
    {
        DontDestroyOnLoad(this);
        foreach (var scriptSingle in _singles)
        {
            if (scriptSingle != null)
            {
                StartSingleton(scriptSingle);
            }
        }
    }

    void StartSingleton(string singletonName)  // Запускаем синглтоны по имени
    {
        string targetTypeName =  singletonName;
        Type targetType = Type.GetType(targetTypeName);
        Type genericType = typeof(Singleton<>); // Тип обобщённого класса
        Type constructedClass = genericType.MakeGenericType(targetType);  // Итоговый Тип 
        var myTypeInstance = Activator.CreateInstance(constructedClass);  // Создаём Класс
        MethodInfo mi = constructedClass.GetMethod("Initialize");  // Берём метод класса.
        mi.Invoke(myTypeInstance, null);  // Запускаем метод для данного класса (он у нас инициализирует ОБЬЪЕКТ!)
    }

    private void OnLevelWasLoaded() // Выполняется при загрузки сцен
    {
        Resources.UnloadUnusedAssets();
        Scene m_Scene;
        m_Scene = SceneManager.GetActiveScene();
        if (m_Scene.name.StartsWith("Level_"))  // Если загружаем уровни
        {
          //  GameState.Instance.timeStarLevel = System.DateTime.Now;
        }
    }
}