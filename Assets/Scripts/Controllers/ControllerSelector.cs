﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerSelector : MonoCashed // Определяет на какой объект нажали мышкой на экране 2D или 3D.
{
    RaycastHit[] _hits;// на что попал рэйкаст.
    RaycastHit2D[] _hits2D; // на что попал рэйкаст.

    void Update()
    {
        if (Input.GetMouseButtonDown(0) & TurnManager.Instance.CurrentPlayer._name == "player")  // Нажата кнопка мышки и ход игрока
        {
            _hits = Physics.RaycastAll(Camera.main.transform.position, Camera.main.ScreenPointToRay(Input.mousePosition).direction, 100.0F);  // Проверяем в какой объект попали
            _hits2D = Physics2D.RaycastAll(Camera.main.transform.position, Camera.main.ScreenPointToRay(Input.mousePosition).direction, 100.0F); // Проверяем в какой  2D объект попали
            CheckHits();
        }
    }

    void CheckHits()  // Проверяем на что нажали мышкой На юнита, на землю или ещё что. (И делаем проверку во что попал луч выделения в первую очередь )
    {
        GameObject hitedUnit = null;
        Sector hitedSector = null;

        List<GameObject> listHittingsObject = new List<GameObject>(); // Список объектов через которые прошел луч
        for (int i = 0; i < _hits.Length; i++) // Для каждого колайдера через который прошел луч шажатия
        {
            RaycastHit hit = _hits[i];
            listHittingsObject.Add(hit.transform.gameObject);
        }
        for (int i = 0; i < _hits2D.Length; i++) // Для каждого 2Dколайдера через который прошел луч шажатия
        {
            RaycastHit2D hit = _hits2D[i];
            listHittingsObject.Add(hit.transform.gameObject);
        }

        GameObject hitLand = listHittingsObject.Find(x => x.tag == "Land");
        GameObject hitUnit = listHittingsObject.Find(x => x.tag.StartsWith("Unit"));

        if (hitUnit != null)  // Делаем проверку во что попали в первую очередь    По уменьшению приоритета  Сначала юниты потом земля
        {
            hitedUnit = hitUnit.transform.parent.gameObject;
            hitedSector = hitedUnit.GetComponentInParent<Unit>()._unitOnSector;
        }
        else if (hitLand != null)
        {
            hitedSector = hitLand.GetComponentInParent<Sector>();
        }
        TakeAction(hitedSector, hitedUnit);
    }


    void TakeAction(Sector hitedSector = null, GameObject hitedUnit = null)  // Выполняем стандартные действия при выборе  юнита или клетки (сектора)
    {
        if (SectorSelector.Instance._selectedSectors.Contains(hitedSector) & SectorSelector.Instance._selectedUnit != null)  // Передвигаем  юнит
        {
            if (!SectorSelector.Instance._selectedUnit.GetComponent<Unit>()._unitIsMoving) // Если текущий юнит не двигается
            {
                List<Sector> pathBySectors = SectorSelector.Instance.MakePathToMove(SectorSelector.Instance._selectedUnit.GetComponent<Unit>()._unitOnSector, hitedSector); // Создаём путь между юнитом и выбранным сектором
                pathBySectors.RemoveAt(pathBySectors.Count -1); // Удаляем точку пути на которой стоит юнит

                if (pathBySectors.Count !=0)
                {
                    SectorSelector.Instance.StartMoveUnitByPath(pathBySectors, SectorSelector.Instance._selectedUnit);  // Отправляем юнита по пути
                    SectorSelector.Instance._selectedSector = null;
                    SectorSelector.Instance._selectedUnit = null;
                    SectorSelector.Instance.ClearMarkSector(SectorSelector.Instance._selectedSectors);// Очищаем список выделенных ранее секторов
                }
            }
        }

        if (hitedUnit != null) // Если  это юнит игрока- запускаем отметку клеток куда пойти - напасть
        {
            SectorSelector.Instance._selectedUnit = hitedUnit;
            if (hitedUnit.transform.tag == "unitPlayer")
            {
                if (!hitedUnit.GetComponent<Unit>().EndTurn & !hitedUnit.GetComponent<Unit>()._unitIsMoving) // Если выбранный юнит может ходить и не двигается сейчас
                {
                    SectorSelector.Instance.GetStartPosition(hitedUnit);
                }
            }
        }
    }

 



}