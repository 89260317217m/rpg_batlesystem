﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;


public class ControllerAI : MonoBehaviour   // Искуственный интелект для боя
{
   // public delegate List<Sector> DelegatSelectAttack(int x, int y, int z, int pointAction);  // Делегат для метода  выбора клеток для атаки

    public void StartTurn()  
    {



         IEnumerator coroutine = DoSomething();
            StartCoroutine(coroutine);



        //for (int i = 0; i < TurnManager.Instance.CurrentPlayer._playerUnits.Count; i++)  // Перебирает всех юнитов текущего игрока (компьютер)
        //{
        //    GameObject unit = TurnManager.Instance.CurrentPlayer._playerUnits[i];

        //    bool tessssttt = unit.GetComponent<Unit>()._unitIsMoving;

        //    //   Найди врагов и иди к ним!

        //    Vector3 position = unit.GetComponent<Unit>()._posOnArray;
        //    int x = (int)position.x;  //Точка в матрице от которой выделяем. В цент стороны ромб и т.д.
        //    int y = (int)position.y;//Точка в матрице от которой выделяем. В цент стороны ромб и т.д.
        //    int z = (int)position.z;//Точка в матрице от которой выделяем. В цент стороны ромб и т.д.

        //    int pointsAction = unit.GetComponent<Unit>()._pointsAction;
        //    int distanseAttack = unit.GetComponent<Unit>()._pointsDistanceAttack;
        //    int zoneAttack = unit.GetComponent<Unit>()._pointsZoneAttack;

        //    DelegatSelectAttack delegatSelectAvailablePath = new DelegatSelectAttack(SectorSelector.Instance.GetAvailablePath); // Создаём делегат на метод
        //    DelegatSelectAttack delegatSelectRomb = new DelegatSelectAttack(SectorSelector.Instance.SelectRomb); // Создаём делегат на метод
        //    DelegatSelectAttack delegatSelectSector = new DelegatSelectAttack(SectorSelector.Instance.SelectSector); // Создаём делегат на метод

        //    List<Sector> sectorsToAtack = new List<Sector>();  // сектора в которые Выделили для хода
        //    sectorsToAtack = SectorSelector.Instance.FiandAvalibleInteractions(x, y, z, pointsAction, distanseAttack, zoneAttack, delegatSelectAvailablePath, delegatSelectRomb, delegatSelectSector);


        //    if (sectorsToAtack.Count >0)  // Если нашел цели
        //    {
        //     Sector targetSector =   AtackByPriority(sectorsToAtack);

        //        List<Sector> pathBySectors = SectorSelector.Instance.MakePathToMove(unit.GetComponent<Unit>()._unitOnSector, targetSector);

        //        if (pathBySectors.Count != 0)
        //        {
        //            SectorSelector.Instance.StartMoveUnitByPath(pathBySectors, unit);  // Отправляем юнита по пути
        //        }

        //        }
        //    else
        //    {
        //        MoveToNeighborEnemy();
        //    }


        //    IEnumerator coroutine = DoSomething(unit.GetComponent<Unit>());
        //    StartCoroutine(coroutine);


        //}

    }



    public Sector AtackByPriority(List<Sector> sectorsToAtack) // Выбор цели для атаки   !! Атакует ближайшего врага.
    {
        List<Sector> priorutyToMove = new List<Sector>(); // Сектора  которые подошли условиям для нападения
        foreach (Sector sector in sectorsToAtack)
        {
            Dictionary<Sector, Unit> ggg = sector._unitToAct.Where(kvp => kvp.Value._playerUnit != TurnManager.Instance.CurrentPlayer).ToDictionary(p => p.Key, p => p.Value);   //Выбираем только те где есть враг

            foreach (KeyValuePair<Sector, Unit> kvp in ggg)
            {
                priorutyToMove.Add(kvp.Key);
            }
        }

        Sector retSector = priorutyToMove.Where( x => x._chekedStage ==   priorutyToMove.Min(v => v._chekedStage)).First(); // Выбираем ближайший
        return retSector;
    }

    void MoveToNeighborEnemy() //Перемещаемся к ближайшему врагу 
    {

    }



    IEnumerator DoSomething()  // Ждём пока юнит доходит
    {
       

        for (int i = 0; i < TurnManager.Instance.CurrentPlayer._playerUnits.Count; i++)  // Перебирает всех юнитов текущего игрока (компьютер)
        {
            GameObject unit = TurnManager.Instance.CurrentPlayer._playerUnits[i];

            bool tessssttt = unit.GetComponent<Unit>()._unitIsMoving;

            //   Найди врагов и иди к ним!

            Vector3 position = unit.GetComponent<Unit>()._posOnArray;
            int x = (int)position.x;  //Точка в матрице от которой выделяем. В цент стороны ромб и т.д.
            int y = (int)position.y;//Точка в матрице от которой выделяем. В цент стороны ромб и т.д.
            int z = (int)position.z;//Точка в матрице от которой выделяем. В цент стороны ромб и т.д.

            int pointsAction = unit.GetComponent<Unit>()._pointsAction;
            int distanseAttack = unit.GetComponent<Unit>()._pointsDistanceAttack;
            int zoneAttack = unit.GetComponent<Unit>()._pointsZoneAttack;

            DelegatSelectAttack delegatSelectAvailablePath = new DelegatSelectAttack(SectorSelector.Instance.GetAvailablePath); // Создаём делегат на метод
            DelegatSelectAttack delegatSelectRomb = new DelegatSelectAttack(SectorSelector.Instance.SelectRomb); // Создаём делегат на метод
            DelegatSelectAttack delegatSelectSector = new DelegatSelectAttack(SectorSelector.Instance.SelectSector); // Создаём делегат на метод

            List<Sector> sectorsToAtack = new List<Sector>();  // сектора в которые Выделили для хода
            sectorsToAtack = SectorSelector.Instance.FiandAvalibleInteractions(x, y, z, pointsAction, distanseAttack, zoneAttack, delegatSelectAvailablePath, delegatSelectRomb, delegatSelectSector);


            if (sectorsToAtack.Count > 0)  // Если нашел цели
            {
                Sector targetSector = AtackByPriority(sectorsToAtack);

                List<Sector> pathBySectors = SectorSelector.Instance.MakePathToMove(unit.GetComponent<Unit>()._unitOnSector, targetSector);

                if (pathBySectors.Count != 0)
                {
                    SectorSelector.Instance.StartMoveUnitByPath(pathBySectors, unit);  // Отправляем юнита по пути
                }

            }
            else
            {
                MoveToNeighborEnemy();
            }

            bool unitMove = unit.GetComponent<Unit>()._unitIsMoving;

            Debug.Log("start");
            while (unit.GetComponent<Unit>()._unitIsMoving)
            {
                yield return new WaitForSeconds(1);
            }
            Debug.Log("end");
        }
      
        
    }





}